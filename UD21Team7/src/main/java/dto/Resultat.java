package dto;

public class Resultat {
	private String memoria;
	private Convertor convertor;
	public Resultat() {
		super();
		this.memoria = "<html>";
		this.convertor=new Convertor();
	}

	public String getMemoria() {
		return memoria+"</html>";
	}

	public void setMemoria(String memoria, Double resultat) {
		double pasar=resultat;
		int convertir=(int)pasar;
		convertor.convertir(convertir);
		this.memoria=this.memoria+"<p>"+memoria+"</p>";
	}
	
	public void resetMemoria() {
		this.memoria="<html>";
	}
	
	public String getBinari() {
		return convertor.getResBinari();
	}
	
	public String getOctal() {
		return convertor.getResOctal();
	}
	public String getHex() {
		return convertor.getResHexadecimal();
	}
}
