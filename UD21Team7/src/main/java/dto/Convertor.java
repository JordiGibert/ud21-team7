package dto;

public class Convertor {
	private int resDecimal;
	private String resBinari, resOctal,resHexadecimal;
	public Convertor() {
		resDecimal=0;
		resOctal="";
		resBinari="";
		resHexadecimal="";
	}
	//funcions propies
	public void convertir(int convertir) {
		resDecimal=convertir;
		resBinari=Integer.toBinaryString(resDecimal);
		resOctal=Integer.toOctalString(resDecimal);
		resHexadecimal=Integer.toHexString(resDecimal);
	}
	//Geters i setters
	public int getResDecimal() {
		return resDecimal;
	}
	public String getResBinari() {
		return resBinari;
	}
	public String getResOctal() {
		return resOctal;
	}
	public String getResHexadecimal() {
		return resHexadecimal;
	}
	public void setResDecimal(int resDecimal) {
		this.resDecimal = resDecimal;
	}
	public void setResBinari(String resBinari) {
		this.resBinari = resBinari;
	}
	public void setResOctal(String resOctal) {
		this.resOctal = resOctal;
	}
	public void setResHexadecimal(String resHexadecimal) {
		this.resHexadecimal = resHexadecimal;
	}
	
	
	
	
	
	

}
