package visible;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.FlowLayout;
import dto.Operacio;
import dto.Resultat;

import javax.swing.JButton;
import java.awt.Color;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JTextPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.border.LineBorder;

public class CalculadoraVisible extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CalculadoraVisible frame = new CalculadoraVisible();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	
	JLabel pantalla = new JLabel("");
	boolean ferOperacio= false;
	Operacio operacion = new Operacio();
	boolean primerPunt = true;
	Resultat memoria = new Resultat();
	JLabel lblMemoria = new JLabel("");
	JLabel lblNumBin = new JLabel("");
	JLabel lblNumOct = new JLabel("");
	JLabel lblNumHex = new JLabel("");
	
	public CalculadoraVisible() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 512, 477);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel historial = new JPanel();
		historial.setBorder(new LineBorder(new Color(0, 0, 0)));
		historial.setBounds(305, 0, 201, 446);
		contentPane.add(historial);
		historial.setLayout(null);
		
		JLabel lblNewLabel_1 = new JLabel("Memoria");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblNewLabel_1.setBounds(10, 11, 83, 14);
		historial.add(lblNewLabel_1);
		
		
		lblMemoria.setFont(new Font("Monospaced", Font.PLAIN, 12));
		lblMemoria.setVerticalAlignment(SwingConstants.TOP);
		lblMemoria.setBounds(0, 36, 191, 406);
		historial.add(lblMemoria);
		
		
		pantalla.setFont(new Font("Monospaced", Font.PLAIN, 30));
		pantalla.setHorizontalAlignment(SwingConstants.RIGHT);
		pantalla.setBounds(0, 0, 305, 69);
		contentPane.add(pantalla);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 169, 305, 277);
		contentPane.add(panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0, 0, 0, 0};
		gbl_panel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0};
		gbl_panel.columnWeights = new double[]{1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{1.0, 1.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JButton btnModul = new JButton("%");
		btnModul.setFont(new Font("Tahoma", Font.PLAIN, 22));
		GridBagConstraints gbc_btnModul = new GridBagConstraints();
		gbc_btnModul.fill = GridBagConstraints.BOTH;
		gbc_btnModul.insets = new Insets(0, 0, 5, 5);
		gbc_btnModul.gridx = 0;
		gbc_btnModul.gridy = 0;
		panel.add(btnModul, gbc_btnModul);
		
		JButton btnCE = new JButton("CE");
		btnCE.setFont(new Font("Tahoma", Font.PLAIN, 22));
		GridBagConstraints gbc_btnCE = new GridBagConstraints();
		gbc_btnCE.fill = GridBagConstraints.BOTH;
		gbc_btnCE.insets = new Insets(0, 0, 5, 5);
		gbc_btnCE.gridx = 1;
		gbc_btnCE.gridy = 0;
		panel.add(btnCE, gbc_btnCE);
		
		JButton btnC = new JButton("C");
		btnC.setFont(new Font("Tahoma", Font.PLAIN, 22));
		GridBagConstraints gbc_btnC = new GridBagConstraints();
		gbc_btnC.fill = GridBagConstraints.BOTH;
		gbc_btnC.insets = new Insets(0, 0, 5, 5);
		gbc_btnC.gridx = 2;
		gbc_btnC.gridy = 0;
		panel.add(btnC, gbc_btnC);
		
		JButton btnBorrar = new JButton("←");
		btnBorrar.setFont(new Font("Tahoma", Font.PLAIN, 22));
		GridBagConstraints gbc_btnBorrar = new GridBagConstraints();
		gbc_btnBorrar.fill = GridBagConstraints.BOTH;
		gbc_btnBorrar.insets = new Insets(0, 0, 5, 0);
		gbc_btnBorrar.gridx = 3;
		gbc_btnBorrar.gridy = 0;
		panel.add(btnBorrar, gbc_btnBorrar);
		
		JButton btnDiv1 = new JButton("1/x");
		btnDiv1.setFont(new Font("Tahoma", Font.PLAIN, 22));
		GridBagConstraints gbc_btnDiv1 = new GridBagConstraints();
		gbc_btnDiv1.fill = GridBagConstraints.BOTH;
		gbc_btnDiv1.insets = new Insets(0, 0, 5, 5);
		gbc_btnDiv1.gridx = 0;
		gbc_btnDiv1.gridy = 1;
		panel.add(btnDiv1, gbc_btnDiv1);
		
		JButton btnQuadrado = new JButton("x²");
		btnQuadrado.setFont(new Font("Tahoma", Font.PLAIN, 22));
		GridBagConstraints gbc_btnQuadrado = new GridBagConstraints();
		gbc_btnQuadrado.fill = GridBagConstraints.BOTH;
		gbc_btnQuadrado.insets = new Insets(0, 0, 5, 5);
		gbc_btnQuadrado.gridx = 1;
		gbc_btnQuadrado.gridy = 1;
		panel.add(btnQuadrado, gbc_btnQuadrado);
		
		JButton btnRaizCuadrada = new JButton("√x");
		btnRaizCuadrada.setFont(new Font("Tahoma", Font.PLAIN, 22));
		GridBagConstraints gbc_btnRaizCuadrada = new GridBagConstraints();
		gbc_btnRaizCuadrada.fill = GridBagConstraints.BOTH;
		gbc_btnRaizCuadrada.insets = new Insets(0, 0, 5, 5);
		gbc_btnRaizCuadrada.gridx = 2;
		gbc_btnRaizCuadrada.gridy = 1;
		panel.add(btnRaizCuadrada, gbc_btnRaizCuadrada);
		
		JButton btnDivision = new JButton("/");
		btnDivision.setFont(new Font("Tahoma", Font.PLAIN, 22));
		GridBagConstraints gbc_btnDivision = new GridBagConstraints();
		gbc_btnDivision.fill = GridBagConstraints.BOTH;
		gbc_btnDivision.insets = new Insets(0, 0, 5, 0);
		gbc_btnDivision.gridx = 3;
		gbc_btnDivision.gridy = 1;
		panel.add(btnDivision, gbc_btnDivision);
		
		JButton btn7 = new JButton("7");
		btn7.setFont(new Font("Tahoma", Font.PLAIN, 22));
		btn7.setBackground(Color.LIGHT_GRAY);
		GridBagConstraints gbc_btn7 = new GridBagConstraints();
		gbc_btn7.fill = GridBagConstraints.BOTH;
		gbc_btn7.insets = new Insets(0, 0, 5, 5);
		gbc_btn7.gridx = 0;
		gbc_btn7.gridy = 2;
		panel.add(btn7, gbc_btn7);
		
		JButton btn8 = new JButton("8");
		btn8.setFont(new Font("Tahoma", Font.PLAIN, 22));
		btn8.setBackground(Color.LIGHT_GRAY);
		GridBagConstraints gbc_btn8 = new GridBagConstraints();
		gbc_btn8.fill = GridBagConstraints.BOTH;
		gbc_btn8.insets = new Insets(0, 0, 5, 5);
		gbc_btn8.gridx = 1;
		gbc_btn8.gridy = 2;
		panel.add(btn8, gbc_btn8);
		
		JButton btn9 = new JButton("9");
		btn9.setFont(new Font("Tahoma", Font.PLAIN, 22));
		btn9.setBackground(Color.LIGHT_GRAY);
		GridBagConstraints gbc_btn9 = new GridBagConstraints();
		gbc_btn9.fill = GridBagConstraints.BOTH;
		gbc_btn9.insets = new Insets(0, 0, 5, 5);
		gbc_btn9.gridx = 2;
		gbc_btn9.gridy = 2;
		panel.add(btn9, gbc_btn9);
		
		JButton btnX = new JButton("X");
		btnX.setFont(new Font("Tahoma", Font.PLAIN, 22));
		GridBagConstraints gbc_btnX = new GridBagConstraints();
		gbc_btnX.fill = GridBagConstraints.BOTH;
		gbc_btnX.insets = new Insets(0, 0, 5, 0);
		gbc_btnX.gridx = 3;
		gbc_btnX.gridy = 2;
		panel.add(btnX, gbc_btnX);
		
		JButton btn4 = new JButton("4");
		btn4.setFont(new Font("Tahoma", Font.PLAIN, 22));
		btn4.setBackground(Color.LIGHT_GRAY);
		GridBagConstraints gbc_btn4 = new GridBagConstraints();
		gbc_btn4.fill = GridBagConstraints.BOTH;
		gbc_btn4.insets = new Insets(0, 0, 5, 5);
		gbc_btn4.gridx = 0;
		gbc_btn4.gridy = 3;
		panel.add(btn4, gbc_btn4);
		
		JButton btn5 = new JButton("5");
		btn5.setFont(new Font("Tahoma", Font.PLAIN, 22));
		btn5.setBackground(Color.LIGHT_GRAY);
		GridBagConstraints gbc_btn5 = new GridBagConstraints();
		gbc_btn5.fill = GridBagConstraints.BOTH;
		gbc_btn5.insets = new Insets(0, 0, 5, 5);
		gbc_btn5.gridx = 1;
		gbc_btn5.gridy = 3;
		panel.add(btn5, gbc_btn5);
		
		JButton btn6 = new JButton("6");
		btn6.setFont(new Font("Tahoma", Font.PLAIN, 22));
		btn6.setBackground(Color.LIGHT_GRAY);
		GridBagConstraints gbc_btn6 = new GridBagConstraints();
		gbc_btn6.fill = GridBagConstraints.BOTH;
		gbc_btn6.insets = new Insets(0, 0, 5, 5);
		gbc_btn6.gridx = 2;
		gbc_btn6.gridy = 3;
		panel.add(btn6, gbc_btn6);
		
		JButton btnMas = new JButton("+");
		btnMas.setFont(new Font("Tahoma", Font.PLAIN, 22));
		GridBagConstraints gbc_btnMas = new GridBagConstraints();
		gbc_btnMas.fill = GridBagConstraints.BOTH;
		gbc_btnMas.insets = new Insets(0, 0, 5, 0);
		gbc_btnMas.gridx = 3;
		gbc_btnMas.gridy = 3;
		panel.add(btnMas, gbc_btnMas);
		
		JButton btn1 = new JButton("1");
		btn1.setFont(new Font("Tahoma", Font.PLAIN, 22));
		btn1.setBackground(Color.LIGHT_GRAY);
		GridBagConstraints gbc_btn1 = new GridBagConstraints();
		gbc_btn1.fill = GridBagConstraints.BOTH;
		gbc_btn1.insets = new Insets(0, 0, 5, 5);
		gbc_btn1.gridx = 0;
		gbc_btn1.gridy = 4;
		panel.add(btn1, gbc_btn1);
		
		JButton btn2 = new JButton("2");
		btn2.setFont(new Font("Tahoma", Font.PLAIN, 22));
		btn2.setBackground(Color.LIGHT_GRAY);
		GridBagConstraints gbc_btn2 = new GridBagConstraints();
		gbc_btn2.fill = GridBagConstraints.BOTH;
		gbc_btn2.insets = new Insets(0, 0, 5, 5);
		gbc_btn2.gridx = 1;
		gbc_btn2.gridy = 4;
		panel.add(btn2, gbc_btn2);
		
		JButton btn3 = new JButton("3");
		btn3.setFont(new Font("Tahoma", Font.PLAIN, 22));
		btn3.setBackground(Color.LIGHT_GRAY);
		GridBagConstraints gbc_btn3 = new GridBagConstraints();
		gbc_btn3.fill = GridBagConstraints.BOTH;
		gbc_btn3.insets = new Insets(0, 0, 5, 5);
		gbc_btn3.gridx = 2;
		gbc_btn3.gridy = 4;
		panel.add(btn3, gbc_btn3);
		
		JButton btnMenos = new JButton("-");
		btnMenos.setFont(new Font("Tahoma", Font.PLAIN, 22));
		GridBagConstraints gbc_btnMenos = new GridBagConstraints();
		gbc_btnMenos.fill = GridBagConstraints.BOTH;
		gbc_btnMenos.insets = new Insets(0, 0, 5, 0);
		gbc_btnMenos.gridx = 3;
		gbc_btnMenos.gridy = 4;
		panel.add(btnMenos, gbc_btnMenos);
		
		JButton btnNegativo = new JButton("±");
		btnNegativo.setBackground(Color.LIGHT_GRAY);
		btnNegativo.setFont(new Font("Tahoma", Font.PLAIN, 22));
		GridBagConstraints gbc_btnNegativo = new GridBagConstraints();
		gbc_btnNegativo.fill = GridBagConstraints.BOTH;
		gbc_btnNegativo.insets = new Insets(0, 0, 0, 5);
		gbc_btnNegativo.gridx = 0;
		gbc_btnNegativo.gridy = 5;
		panel.add(btnNegativo, gbc_btnNegativo);
		
		JButton btn0 = new JButton("0");
		btn0.setFont(new Font("Tahoma", Font.PLAIN, 22));
		btn0.setBackground(Color.LIGHT_GRAY);
		GridBagConstraints gbc_btn0 = new GridBagConstraints();
		gbc_btn0.fill = GridBagConstraints.BOTH;
		gbc_btn0.insets = new Insets(0, 0, 0, 5);
		gbc_btn0.gridx = 1;
		gbc_btn0.gridy = 5;
		panel.add(btn0, gbc_btn0);
		
		JButton btnComa = new JButton(",");
		btnComa.setFont(new Font("Tahoma", Font.PLAIN, 22));
		btnComa.setBackground(Color.LIGHT_GRAY);
		GridBagConstraints gbc_btnComa = new GridBagConstraints();
		gbc_btnComa.fill = GridBagConstraints.BOTH;
		gbc_btnComa.insets = new Insets(0, 0, 0, 5);
		gbc_btnComa.gridx = 2;
		gbc_btnComa.gridy = 5;
		panel.add(btnComa, gbc_btnComa);
		
		JButton btnIgual = new JButton("=");
		btnIgual.setFont(new Font("Tahoma", Font.PLAIN, 22));
		GridBagConstraints gbc_btnIgual = new GridBagConstraints();
		gbc_btnIgual.fill = GridBagConstraints.BOTH;
		gbc_btnIgual.gridx = 3;
		gbc_btnIgual.gridy = 5;
		panel.add(btnIgual, gbc_btnIgual);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel_1.setBounds(0, 64, 305, 100);
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblBin = new JLabel("BIN:");
		lblBin.setFont(new Font("SimSun", Font.BOLD, 17));
		lblBin.setBounds(0, 11, 46, 25);
		panel_1.add(lblBin);
		
		JLabel lblOct = new JLabel("OCT:");
		lblOct.setFont(new Font("SimSun", Font.BOLD, 17));
		lblOct.setBounds(0, 39, 46, 25);
		panel_1.add(lblOct);
		
		
		lblNumBin.setFont(new Font("Monospaced", Font.PLAIN, 20));
		lblNumBin.setHorizontalAlignment(SwingConstants.LEFT);
		lblNumBin.setBounds(40, 5, 255, 32);
		panel_1.add(lblNumBin);
		
		lblNumOct.setHorizontalAlignment(SwingConstants.LEFT);
		lblNumOct.setFont(new Font("Monospaced", Font.PLAIN, 20));
		lblNumOct.setBounds(40, 33, 255, 32);
		panel_1.add(lblNumOct);
		
		JLabel lblHex = new JLabel("HEX:");
		lblHex.setFont(new Font("SimSun", Font.BOLD, 17));
		lblHex.setBounds(0, 67, 46, 25);
		panel_1.add(lblHex);
		
		lblNumHex.setHorizontalAlignment(SwingConstants.LEFT);
		lblNumHex.setFont(new Font("Monospaced", Font.PLAIN, 20));
		lblNumHex.setBounds(40, 61, 255, 32);
		panel_1.add(lblNumHex);
		

		
		btn1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				afegirPantalla('1');
			}
		});
		
		btn2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				afegirPantalla('2');
			}
		});
		
		btn3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				afegirPantalla('3');
			}
		});
		
		btn4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				afegirPantalla('4');
			}
		});
		
		btn5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				afegirPantalla('5');
			}
		});
		
		btn6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				afegirPantalla('6');
			}
		});
		
		btn7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				afegirPantalla('7');
			}
		});
		btn8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				afegirPantalla('8');
			}
		});
		btn9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				afegirPantalla('9');
			}
		});
		btn0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				afegirPantalla('0');
			}
		});
		btnComa.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(primerPunt==false) {
				afegirPantalla('.');
				}
			}
		});
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!pantalla.getText().isEmpty()) {
				String borrar =pantalla.getText();
				int numBorrar =borrar.length()-1;
				pantalla.setText(borrar.substring(0,numBorrar));
				}
			}
		});
		
		btnC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pantalla.setText("");
				primerPunt=true;
				ferOperacio=false;
				lblNumHex.setText("");
				lblNumOct.setText("");
				lblNumBin.setText("");
			}
		});
		
		btnMas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(primerPunt==false) {
				operacio('+');
				}
			}
		});
		
		btnMenos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(primerPunt==false) {
				operacio('-');
				}
			}
		});
		
		btnDivision.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(primerPunt==false) {
				operacio('/');
				}
			}
		});
		btnX.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(primerPunt==false) {
				operacio('x');
				}
			}
		});
		
		btnRaizCuadrada.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(primerPunt==false) {
				operacioUnica('R');
				}
			}
		});
		btnQuadrado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(primerPunt==false) {
				operacioUnica('2');
				}
			}
		});
		btnModul.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(primerPunt==false) {
				operacio('%');
				}
			}
		});
		
		btnIgual.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(primerPunt==false) {
				primerPunt=true;
				ferOperacio=false;
				Double num1 = Double.parseDouble(pantalla.getText());
				operacion.setNum2(num1);
				pantalla.setText(""+operacion.getResultat());
				lblMemoriaSet();
				operacion.setNum1(operacion.getResultat());
				}
				
			}
		});
		btnDiv1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(primerPunt==false) {
				operacioUnica('1');
				}
			}
		});
		
		btnCE.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pantalla.setText("");
				primerPunt=true;
				ferOperacio=false;
				memoria.resetMemoria();
				lblMemoria.setText("");
				lblNumHex.setText("");
				lblNumOct.setText("");
				lblNumBin.setText("");
			}
		});
		
		btnNegativo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(primerPunt==false) {
				String neg =pantalla.getText();
				int numNeg =neg.length();
				if(neg.substring(0,1).contentEquals("-")) {
					pantalla.setText(neg.substring(1,numNeg));
				} else {
					pantalla.setText('-'+neg);
				}
				}
			}
		});
	}
	
	void afegirPantalla(char x) {
		if(primerPunt) {
			primerPunt=false;
			pantalla.setText("");
		} 
		pantalla.setText(pantalla.getText()+x);
	}
	
	void operacio(char op) {
		if(ferOperacio==true) {
			Double num1 = Double.parseDouble(pantalla.getText());
			operacion.setNum2(num1);
			pantalla.setText(""+operacion.getResultat());
			lblMemoriaSet();
			operacion.setNum1(operacion.getResultat());
			operacion.setOperacio(op);
			
			
		} else {
			Double num1 = Double.parseDouble(pantalla.getText());
			operacion.setNum1(num1);
			operacion.setOperacio(op);
			pantalla.setText("");
			ferOperacio=true;
		}
		primerPunt=true;
	}
	
	void lblMemoriaSet() {
		memoria.setMemoria(""+operacion.getNum1()+operacion.getOperacio()+operacion.getNum2()+"="+operacion.getResultat(),operacion.getResultat());
		lblNumBin.setText(memoria.getBinari());
		lblNumOct.setText(memoria.getOctal());
		lblNumHex.setText(memoria.getHex());
		lblMemoria.setText(memoria.getMemoria());
	}
	
	void lblMemoriaUnicaSet(Operacio prova) {
		memoria.setMemoria(""+prova.getNum1()+prova.getOperacio()+"="+prova.getResultat(),prova.getResultat());
		lblNumBin.setText(memoria.getBinari());
		lblNumOct.setText(memoria.getOctal());
		lblNumHex.setText(memoria.getHex());
		lblMemoria.setText(memoria.getMemoria());
	}
	
	public void operacioUnica(char op) {
		Double num1 = Double.parseDouble(pantalla.getText());
		Operacio prova = new Operacio(num1,op);
		pantalla.setText(""+prova.getResultat());
		primerPunt=true;
		lblMemoriaUnicaSet(prova);
	}
}
